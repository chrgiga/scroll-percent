# Scroll Percent #
**URL Plugin:** http://chrgiga.com/scrollpercent

**Author:** Christian Gil

**Donate link:** https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=XG8CX646FGWP4

**Requires:** jQuery library

### Description ###
Displays the percentage of scroll of an element or page when we doing scroll

### Running the Plugin ###

First off include jQuery and the scrollpercent.min.js file

Secondly you need call the function `scrollpercent` in your script

```
#!javascript

$().scrollPercent();
```

If you want you can indicate the element or elements that you want apply the function `scrollpercent`

```
#!javascript

$(window).scrollPercent(); or $('#myElementWithScroll, textarea').scrollPercent();
```

Furthermore you can customize the label where the info of actual percentage is displayed.
You can indicate the id of the label, all parameters of CSS and the 'manual' attribute, if value of the 'manual' attribute is true, the label will not scroll automatically, and if value of the 'manual' attribute is false or not specified, then the label will scroll automatically:

```
#!javascript

$().scrollPercent({id:'thePercentageLabelId', css{'backgorund-color': '#333', color: '#fff', top: '0'}, manual:true});
```

For more information and see all posibilities, go to http://chrgiga.com/scrollpercent